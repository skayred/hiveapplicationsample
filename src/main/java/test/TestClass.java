package test;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 9/1/13
 * Time: 12:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestClass {
    public Integer add(Integer a, Integer b) {
        return new Adder().add(a, b);
    }

    public Double mult(Double a, Double b) {
        return Multiplicator.mult(a, b);
    }
}
